#!/usr/bin/env python
import rospy
from std_msgs.msg import *
from geometry_msgs.msg import PoseStamped	
from apriltag_ros.msg import AprilTagDetection, AprilTagDetectionArray
from master_package.msg import TagInfo, TagsInfo

tagsinfo_publisher=None

def setup(rospy,april_tag_topic,tagsinfo_pub):
   global tagsinfo_publisher
   tagsinfo_publisher=tagsinfo_pub
   rospy.Subscriber(april_tag_topic, AprilTagDetectionArray, apriltag_callback) 


def apriltag_callback(msg):
   if len(msg.detections)>0:
     tags = TagsInfo()
     tags.header.stamp = rospy.Time.now()
     tags.header.frame_id = msg.header.frame_id
     tags.tag_type="april_tag"
     tags.tags = []
     for item in msg.detections:
       tag=TagInfo()
       tag.id=item.id[0]
       tag.pose=PoseStamped()
       tag.pose.header=item.pose.header
       tag.pose.pose=item.pose.pose.pose
       tags.tags.append(tag)
     tagsinfo_publisher.publish(tags);   
       
     

