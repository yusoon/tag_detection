#!/usr/bin/env python 
import rospy
from std_msgs.msg import *
from geometry_msgs.msg import PoseStamped	
from apriltag_ros.msg import AprilTagDetection, AprilTagDetectionArray
from master_package.msg import TagInfo, TagsInfo
import apriltag_handler

class Tag_detection_publisher():
    def __init__(self):
        rospy.init_node("tag_detection_publisher", anonymous=False)
        self.april_tag_topic = rospy.get_param('~april_tag_topic',"")
        self.tagsinfo_pub = rospy.Publisher("tagsinfo", TagsInfo, queue_size=10) 
          
        if self.april_tag_topic!="":
          apriltag_handler.setup(rospy,self.april_tag_topic,self.tagsinfo_pub)
              
        rospy.spin()
               
       
           
        
if __name__ == '__main__':
    try:
        Tag_detection_publisher()
    except rospy.ROSInterruptException:
        rospy.loginfo("Node has been signaled for shutdown.")
